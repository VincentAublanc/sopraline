﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data.SqlClient;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        

        protected void Page_Load(object sender, EventArgs e) //premier chargement de la page
        {
            if (!Page.IsPostBack) 
            {
                //Vider la combo

                cboPays.Items.Clear();

            }
            SqlConnection connexion = new SqlConnection("Data Source=VINCENT\\SQLEXPRESS;Initial Catalog=Sopraline;Integrated Security=True");
            try
            {
                //Charger la liste des pays

                connexion.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = connexion;

                cmd.CommandText = "Select * FROM T_pays";
                SqlDataReader rs = cmd.ExecuteReader();

                while(rs.Read())
                {
                    ListItem li = new ListItem(); //Création d'une ligne

                    li.Value = rs["alpha3"].ToString(); //valeur masquée
                    li.Text = rs["Nom_FR"].ToString(); //valeur affichée

                    cboPays.Items.Add(li);
                }

                rs.Close();
            }
            catch (Exception ex)
            {
                //spMessage.InnerText = "Erreur de connection à la base";
                spMessage.InnerText = ex.Message;
            }
            finally
            {
                connexion.Close();
            }
        }

        protected void cmdCreer_Click(object sender, EventArgs e)
        {
            // Data Source=VINCENT\SQLEXPRESS;Initial Catalog=Sopraline;Integrated Security=True

            bool erreur = false;
            spMessage.InnerText = "";

            String sNom = txtNom.Text;
            String sPrenom = txtPrenom.Text;
            DateTime sDateDeNaissance = txtDateDeNaissance.SelectedDate;
            String sAdresse = txtAdresse.Text;
            String sCodePostale = txtCodePostale.Text;
            String sVille = txtVille.Text;
            String sPays = cboPays.Text;
            String sTelFixe = txtTelFixe.Text;
            String sTelPerso = txtTelPerso.Text;
            String sEmail = txtEmail.Text;
            String sLogin = txtLogin.Text;
            String sMdp = txtMdp.Text;



            if (sNom == "")
            {
                spMessage.InnerText = "Fout ton nom BORDEL !!!";
                txtNom.Focus();
                return;
            }

            /*if (sPrenom == "")
            {
                spMessage.InnerText = "Fout ton Prenom BORDEL !!!";
                txtPrenom.Focus();
                return;
            }

            /*if (sDateDeNaissance == "")
            {
                spMessage.InnerText = "Fout ta DateDeNaissance BORDEL !!!";
                txtDateDeNaissance.Focus();
                return;
            }*/

            /*if (sAdresse == "")
            {
                spMessage.InnerText = "Fout ton Adresse BORDEL !!!";
                txtAdresse.Focus();
                return;
            }

            if (sCodePostale == "")
            {
                spMessage.InnerText = "Fout ton CodePostale BORDEL !!!";
                txtCodePostale.Focus();
                return;
            }

            if (sVille == "")
            {
                spMessage.InnerText = "Fout ta Ville BORDEL !!!";
                txtVille.Focus();
                return;
            }

            if (sPays == "")
            {
                spMessage.InnerText = "Fout ton Pays BORDEL !!!";
                txtPays.Focus();
                return;
            }

            if (sTelFixe == "")
            {
                spMessage.InnerText = "Fout ton TelFixe BORDEL !!!";
                txtTelFixe.Focus();
                return;
            }

            if (sTelPerso == "")
            {
                spMessage.InnerText = "Fout ton TelPerso BORDEL !!!";
                txtTelPerso.Focus();
                return;
            }

            if (sEmail == "")
            {
                spMessage.InnerText = "Fout ton Email BORDEL !!!";
                txtEmail.Focus();
                return;
            }

            if (sLogin == "")
            {
                spMessage.InnerText = "Fout ton Login BORDEL !!!";
                txtLogin.Focus();
                return;
            }

            if (sMdp == "")
            {
                spMessage.InnerText = "Fout ton Mdp BORDEL !!!";
                txtMdp.Focus();
                return;
            } */



            SqlConnection connexion = new SqlConnection("Data Source=VINCENT\\SQLEXPRESS;Initial Catalog=Sopraline;Integrated Security=True");

            try
            {
                connexion.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = connexion;

                cmd.CommandText = "Select * FROM T_comptes WHERE login='" + sLogin + "' AND mdp='" + sMdp + "'";
                SqlDataReader rs = cmd.ExecuteReader();

                if (rs.Read())
                {
                    spMessage.InnerText = "Le couple Login / Mot de passe est déjà utilisé";
                    erreur = true;
                }
                rs.Close();

                if (!erreur) {
                    cmd.CommandText = "INSERT INTO T_comptes VALUES('" + sNom + "','" + sPrenom + "','" + sDateDeNaissance + "','" + sAdresse + "','" + sCodePostale + "','" + sVille + "','" + sPays + "','" + sTelFixe + "','" + sTelPerso + "','" + sEmail + "','" + sLogin + "','" + sMdp + "')";
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                //spMessage.InnerText = "Erreur de connection à la base";
                spMessage.InnerText = ex.Message;
            }
            finally
            {
                connexion.Close();
            }
        }
    }
}