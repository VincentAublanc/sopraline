﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="catalogue.aspx.cs" Inherits="WebApplication1.WebForm4" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Catalogue SOPRALINE</title>
    <link href="catalogue.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">

        <div class="title">
        <nav>Catalogue Sopraline</nav><br />
        <nav id="spMessageBienvenue" runat="server"></nav>
        </div>

        <div class="lienPanier">
            <asp:Button ID="cmdPanier" runat="server" Text="Panier" /> <!-- OnClick="cmdPanier_Click"-->
        </div>

        <div class="typeBarreProduit">
            <div class="typeProduit">Chocolat Noir</div>
            <div class="typeProduit">Chocolat au Lait</div>
            <div class="typeProduit">Chocolat Blanc</div>
            <div class="typeProduit">Praline</div>
        </div>

        <div id="mainid" class="mainid" runat="server" >

             <div id="titre"  class="titre" runat="server">
                <div class="titreProduits">Produits</div>
                <div class="titrePhotos">Photos</div>
                <div class="titreDescriptifs">Descriptifs</div>
                <div class="titrePrix">Prix</div>
                <div class="titrePanier">Panier</div>
            </div>

            <!--Ajout des div de la BDD (via c#)-->

        </div>
        <div>
            <span id="spMessageCatalogue" runat="server"></span>
        </div>
    </form>
</body>
</html>
