﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;

namespace WebApplication1
{
    public partial class WebForm4 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string sPrenom = Request.QueryString["Prenom"].ToString();
            spMessageBienvenue.InnerText = ("Bienvenue" + sPrenom);

            SqlConnection connexion = new SqlConnection("Data Source=VINCENT\\SQLEXPRESS;Initial Catalog=Sopraline;Integrated Security=True");
            try
            {
                connexion.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = connexion;

                cmd.CommandText = "Select * FROM T_Produits";
                SqlDataReader rs = cmd.ExecuteReader();

                while (rs.Read())
                {
                    HtmlGenericControl divLigne = new HtmlGenericControl("div");
                    HtmlGenericControl divNom = new HtmlGenericControl("div");
                    HtmlGenericControl divDescriptif = new HtmlGenericControl("div");
                    HtmlGenericControl divPrix = new HtmlGenericControl("div");
                    HtmlGenericControl divPhoto = new HtmlGenericControl("div");
                    HtmlGenericControl divPanier = new HtmlGenericControl("div");
                    HtmlSelect cboPanier = new HtmlSelect();

                    divLigne.Controls.Add(divNom);
                    divLigne.Controls.Add(divPhoto);
                    divLigne.Controls.Add(divDescriptif);
                    divLigne.Controls.Add(divPrix);
                    divLigne.Controls.Add(divPanier);
                    divPanier.Controls.Add(cboPanier);

                    divLigne.Attributes["class"] = "données";
                    divPhoto.Attributes["class"] = "photos";
                    divNom.Attributes["class"] = "produits";
                    divDescriptif.Attributes["class"] = "descriptifs";
                    divPrix.Attributes["class"] = "prix";
                    divPanier.Attributes["class"] = "panier";

                    divNom.InnerText = rs["nomProduit"].ToString();
                    divDescriptif.InnerText = rs["descriptifProduit"].ToString();
                    divPrix.InnerText = rs["prixProduit"].ToString();

                    for(int i=0; i<=10; i++ )
                        {
                            ListItem li = new ListItem();
                            li.Text = i.ToString();
                            cboPanier.Items.Add(li);
                        }
                
                    mainid.Controls.Add(divLigne);
                }

                rs.Close();
            }
            
            catch (Exception ex)
            {
                //spMessage.InnerText = "Erreur de connection à la base";
                spMessageCatalogue.InnerText = ex.Message;
            }
            finally
            {
                connexion.Close();
            }
        }

        /*protected void cmdCatalogue_Click(object sender, EventArgs e)
        {
            Response.Redirect("panier.aspx");
        }*/
    }
}