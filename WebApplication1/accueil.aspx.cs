﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data.SqlClient;

namespace WebApplication1
{
    public partial class WebForm2 : System.Web.UI.Page
    {        

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void cmdInscription_Click(object sender, EventArgs e)
        {
            //Response.Write("<div style='color:red'>Hello</div>");
            Response.Redirect("formulaire.aspx");
        }

        protected void cmdCatalogue_Click(object sender, EventArgs e)
        {
            Response.Redirect("catalogue.aspx");
        }

        protected void cmdConnection_Click(object sender, EventArgs e)
        {
            SqlConnection connexion = new SqlConnection("Data Source=VINCENT\\SQLEXPRESS;Initial Catalog=Sopraline;Integrated Security=True");
            String aLogin = txtLoginAccueil.Text;
            String aMdp = txtMdpAccueil.Text;
            String bddMdp="";

            try
            {
                connexion.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = connexion;

                cmd.CommandText = "Select * FROM T_comptes WHERE Login='"+ aLogin +"'";
                SqlDataReader rs = cmd.ExecuteReader();

                if (rs.Read())
                {
                    bddMdp = rs["Mdp"].ToString();
                }
                else
                {
                    spMessageAccueil.InnerText = "Login inexistant";
                }

                if(aMdp == bddMdp)
                {
                    Session["Prenom"] = rs["Prenom"].ToString();
                    Response.Redirect("catalogue.aspx?Prenom="+ Session["Prenom"].ToString());
                }
                else
                {
                    spMessageAccueil.InnerText = "Mot de passe incorrect pour ce compte";
                }
                rs.Close();

            }
            catch (Exception ex)
            {
                //spMessage.InnerText = "Erreur de connection à la base";
                spMessageAccueil.InnerText = ex.Message;
            }
            finally
            {
                connexion.Close();
            }
        }
    }
}