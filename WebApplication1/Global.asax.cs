﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace WebApplication1
{
    public class Global1 : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            Application["connectionstring"] = "Data Source=VINCENT\\SQLEXPRESS;Initial Catalog=Sopraline;Integrated Security=True";
    }

        protected void Session_Start(object sender, EventArgs e)
        {
            Session["Prenom"] = "";
            ArrayList panier = new ArrayList();
            Session["panier"] = panier;
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}