﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="formulaire.aspx.cs" Inherits="WebApplication1.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="StyleSheet1.css" rel="stylesheet" />
</head>
<body>
    <form id="formCompte" runat="server">
    <div class="pageFormulaire">
        <table class="formulaire">
            <tr>
                <td class="colonne1">Nom</td>
                <td class="colonne2"><asp:TextBox ID="txtNom" type="text" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="colonne1">Prenom</td>
                <td class="colonne2"><asp:TextBox ID="txtPrenom" type="text" runat="server"></asp:TextBox></td>
            </tr>            
            <tr>
                <td class="colonne1">Date de naissance</td>
                <td class="colonne2"><asp:Calendar ID="txtDateDeNaissance" runat="server" ></asp:Calendar></td>
            </tr>            
            <tr>
                <td class="colonne1">Adresse</td>
                <td class="colonne2"><asp:TextBox ID="txtAdresse" type="text" runat="server"></asp:TextBox></td>
            </tr>            
            <tr>
                <td class="colonne1">Code Postale</td>
                <td class="colonne2"><asp:TextBox ID="txtCodePostale" type="text" runat="server"></asp:TextBox></td>
            </tr>           
             <tr>
                <td class="colonne1">Ville</td>
                <td class="colonne2"><asp:TextBox ID="txtVille" type="text" runat="server"></asp:TextBox></td>
            </tr>            
            <tr>
                <td class="colonne1">Pays</td>
                <td class="colonne2">
                    <asp:DropDownList id="cboPays" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>            
            <tr>
                <td class="colonne1">Telephone Fixe</td>
                <td class="colonne2"><asp:TextBox ID="txtTelFixe" type="text" runat="server"></asp:TextBox></td>
            </tr>            
            <tr>
                <td class="colonne1">Telephone Personnel</td>
                <td class="colonne2"><asp:TextBox ID="txtTelPerso" type="text" runat="server"></asp:TextBox></td>
            </tr>            
            <tr>
                <td class="colonne1">E-mail</td>
                <td class="colonne2"><asp:TextBox ID="txtEmail" type="text" runat="server"></asp:TextBox></td>
            </tr>            
            <tr>
                <td class="colonne1">Login</td>
                <td class="colonne2"><asp:TextBox ID="txtLogin" type="text" runat="server"></asp:TextBox></td>
            </tr>            
            <tr>
                <td class="colonne1">Mot de passe</td>
                <td class="colonne2"><asp:TextBox ID="txtMdp" type="text" runat="server" TextMode="Password"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="cmdCreer" runat="server" Text="Creer compte" OnClick="cmdCreer_Click"/>
                </td>   
            </tr>
            <tr>
                <td colspan="2">
                    <span id="spMessage" runat="server"></span>
                </td>   
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
