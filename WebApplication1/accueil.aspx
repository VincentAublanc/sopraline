﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="accueil.aspx.cs" Inherits="WebApplication1.WebForm2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Button ID="cmdInscription" runat="server" Text="S'inscrire" OnClick="cmdInscription_Click" />
    </div>
    <div style="margin-top:20px">
        <nav>Login</nav>
        <asp:TextBox ID="txtLoginAccueil" type="text" runat="server"></asp:TextBox>
        <nav>Mot de Passe</nav>
        <asp:TextBox ID="txtMdpAccueil" type="text" runat="server"></asp:TextBox><br /><br />
        <asp:Button ID="cmdConnection" runat="server" Text="Se Connecter" OnClick="cmdConnection_Click"/>
        <span id="spMessageAccueil" runat="server"></span>
    </div>
    </form>
</body>
</html>
